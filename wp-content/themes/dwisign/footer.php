	<div class="footer">
	  <div class="container">
	  	<div class="row">
	      <div class="col-md-3 col-sm-6 logo-footer">
	        <!-- </br><img src="<?php //bloginfo('template_directory'); ?>/image/footer_logo.png" width="120"> -->
	      </div>
	  		
	      <div class="col-md-3 col-sm-6">
	        <?php if ( is_active_sidebar( 'footer_2' ) ) : ?> 
			    <?php dynamic_sidebar( 'footer_2' ); ?>
			<?php endif; ?>
	      </div>
	  	  <div class="col-md-3 col-sm-6">
  			<?php if ( is_active_sidebar( 'footer_3' ) ) : ?> 
			    <?php dynamic_sidebar( 'footer_3' ); ?>
			<?php endif; ?>
	  	  </div>
	      <div class="col-md-3 col-sm-6">
	        <?php if ( is_active_sidebar( 'footer_4' ) ) : ?> 
			    <?php dynamic_sidebar( 'footer_4' ); ?>
			<?php endif; ?>
	      </div>
	  	</div>   
	  </div>
	</div>

	<div class="footer-bottom">
	  <div class="container">
	    <p>Copyright <?php echo date( 'Y' ) ?> | Your Site</p>
	  </div>
	</div>

	<?php wp_footer(); ?>
	</body>
</html>