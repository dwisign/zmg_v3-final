<?php get_header(); ?>

    <?php
      $args = array(
        'post_type' => 'service'
      );
      $service = new WP_Query( $args );
    ?>

    <?php
      $args = array(
        'post_type' => 'partner'
      );
      $partner = new WP_Query( $args );
    ?>

    <!--BANNER-->
    <div class="banner">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <div id="bannerSlider" class="owl-carousel">
                <?php if ( $service->have_posts () ) : while ( $service->have_posts () ) : $service->the_post ();?>
                  <div class="item"><img src="<?php the_field('item_image');?>" alt="zmgServices"></div>
                <?php endwhile; else: ?>
                <?php endif; ?>
            </div>   
          </div>
          <div class="col-md-4">
            <div class="right-side">
              <h5 class="title">ZMG AT A GLANCE</h5><hr>            
              <p>
                ZMG Telekomunikasi Servise Indonesia the leading professional 
                firm known as ZMG since 2008 in Jakarta, which oriented business 
                in Engineering and Construction of a global cellular mobile 
                communication system. 
              </p>
              </br>
              <a href="http://zmgv3.zmgland.com/about-us/" type="submit" class="btn btn-info btn-block btn-sm">READ MORE</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--END BANNER--> 


    <!--SERVICES--> 
    <div class="services">
      <div class="container">
        <div class="row">
          <!--LEFT SIDE--> 
          <div class="col-sm-8">
            <div class="left-side">
              <!-- SLIDER SERVICES -->
              <h5 class="title"><span class="glyphicon glyphicon-heart" aria-hidden="true"></span>&nbsp OUR SERVICES</h5><hr>
              <div id="serviceSlider">
                <?php if ( $service->have_posts () ) : while ( $service->have_posts () ) : $service->the_post ();?>        
                <div class="item">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="feature-image-service"><?php the_post_thumbnail(); ?></div>
                    </div>
                    <div class="col-lg-6 desc-serv">
                        <h5 class="title"><?php the_field('item_title');?></h5><hr>
                        <p>
                          <?php the_field('item_desc');?> 
                        </p>
                    </div>
                  </div>   
                </div>
                <?php endwhile; else: ?>
                <?php endif; ?>
              </div>
              <!-- END SLIDER SERVICES -->

              <!-- AFFILIATE -->
              </br></br><h5 class="title"><span class="glyphicon glyphicon-link" aria-hidden="true"></span>&nbsp AFFILIATE</h5><hr>
              <div class="row">
                <div class="col-md-3 col-sm-6 text-center">
                  <a href="http://ipapa.co.id" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/image/ipapaLogoGrey.png" width="140"></a></br></br>
                </div>
                <div class="col-md-3 col-sm-6 text-center">
                  <a href="#"><img src="<?php bloginfo('template_directory'); ?>/image/zmgLogoGrey-1.png" width="150"></a></br></br>
                </div>
                <div class="col-md-3 col-sm-6 text-center">
                  <a href="#"><img src="<?php bloginfo('template_directory'); ?>/image/zmgLogoGrey-2.png" width="150"></a></br></br>
                </div>
                <div class="col-md-3 col-sm-6 text-center">
                  <a href="#"><img src="<?php bloginfo('template_directory'); ?>/image/zmgLogoGrey-3.png" width="150"></a></br></br>
                </div>
              </div>
              <!-- END AFFILIATE -->
            </div>
          </div>
          <!--END LEFT SIDE--> 

          <!--RIGHT SIDE-->
          <div class="col-sm-4">
            <div class="right-side">
              <!-- RIGHT SIDE WIDGET -->
              <?php get_sidebar(); ?>
              <!-- END RIGHT SIDE WIDGET -->
            </div>
          </div>
          <!--END RIGHT SIDE-->
        </div>
      </div><!--END CONTAINER-->
    </div> 
    <!--END SERVICES--> 


    <!--QUOTE--> 
    <div class="quote">
      <div class="container">
        <h4>"We are growing at exciting rates and have great plans for growth and development"</h4>
      </div>
    </div>
    <!--END QUOTE--> 


    <!--PARTNER--> 
    <div class="partner">
      <div class="container">
        <h5 class="title"><span class="glyphicon glyphicon-transfer" aria-hidden="true"></span>&nbsp OUR PARTNER</h5>
        <div id="partnerSlider">
          <?php if ( $partner->have_posts () ) : while ( $partner->have_posts () ) : $partner->the_post ();?>
            <div class="item"><img src="<?php the_field('upload_partner_logo');?>" width="100"></div>
          <?php endwhile; else: ?>
          <?php endif; ?>         
        </div>  
      </div>
    </div>
    <!--END PARTNER-->



<?php get_footer(); ?>



