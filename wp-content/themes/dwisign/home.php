<?php get_header(); ?>
	
	<!-- BANNER INSIDE -->
    <div class="banner banner-inside">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <div class="right-side">
              <h4 class="title"><span class="glyphicon glyphicon-star" aria-hidden="true"></span>&nbsp CAREER</h4><hr>            
              <p class="desc-title-inside">
                "We are a fast growing company now is seeking for best 
                candidates to join us for promotion together" 
              </p>
            </div>
          </div>
          <div class="col-lg-8">
            <img src="<?php bloginfo('template_directory'); ?>/image/banner-inside-3.jpg" width="100%">   
          </div>
        </div>
      </div>
    </div>
    <!-- END BANNER INSIDE --> 

	<!-- CONTENT TWO COLUMN -->
    <div class="content-two-column">
      <div class="container">
        <div class="row">
          <!-- LEFT SIDE -->
          <div class="col-sm-8">
            <div class="left-side">
              <!-- <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">Career</li>
              </ol> -->
              <?php //get_breadcrumb(); ?><h4 class="title">Available Positions</h4><hr>
              <!-- MAIN JOB LIST -->
              <div class="list-group">
              	<?php if ( have_posts () ) : while ( have_posts () ) : the_post ();?>
	                <a href="<?php the_permalink();?>" class="list-group-item">
	                  <h4 class="list-group-item-heading"><?php the_title(); ?></h4>
	                  <p class="list-group-item-text"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> <?php the_field( 'location' );?> | <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Valid Until : <?php the_field( 'close_date' );?></p>
	                </a>
                <?php endwhile; else: ?>
                <?php endif; ?>
              </div>
              <!-- END JOB LIST -->

              <!-- PAGINATION -->
              <div class="row">
                <div class="col-xs-12 text-center">
                  <nav class="pagination">
                    <?php wp_bs_pagination(); ?>
                  </nav>
                </div>
              </div>
              <!-- END PAGINATION -->
            </div>
          </div>
          <!-- END LEFT SIDE -->

          <!-- RIGHT SIDE -->
          <div class="col-sm-4">
            <div class="right-side">
              <!-- RIGHT SIDE WIDGET -->
              <?php get_sidebar(); ?>
              <!-- END RIGHT SIDE WIDGET -->
            </div>
          </div>
          <!-- END RIGHT SIDE -->
        </div>
      </div><!-- END CONTAINER -->
    </div>
    <!-- END CONTENT TWO COLUMN -->

<?php get_footer(); ?>