<!DOCTYPE html>
<html lang="en">
  <head>
  		<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1">
  		<title><?php wp_title( '|', true, 'right' ); bloginfo( 'name' ) ?></title>
  		<?php wp_head(); ?>
  </head>

  <body>
  <div class="phone-bar">
	  <div class="container">
	    <span class="glyphicon glyphicon-phone-alt" aria-hidden="true"></span> +62 21 300 66126  &nbsp &nbsp &nbsp <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> mail@site.co.id 
	  </div>
	</div>

	<nav class="navbar navbar-white">
	  <div class="container">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>      
	      <a class="navbar-brand" href="<?php //bloginfo('url')?>"><!--<img src="<?php bloginfo('template_directory'); ?>/image/ZMG_logo.png" width="60">--></a> 
	    </div>

	    <?php
            wp_nav_menu( array(
                'menu'              => 'primary',
                'theme_location'    => 'primary',
                'depth'             => 2,
                'container'         => 'div',
                'container_class'   => 'collapse navbar-collapse',
        		'container_id'      => 'bs-example-navbar-collapse-2',
                'menu_class'        => 'nav navbar-nav navbar-right',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker())
            );
        ?>
	  </div>
	</nav>

