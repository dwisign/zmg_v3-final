<?php
add_action( 'init', 'custom_post_calculate' );
add_action( 'cmb2_admin_init', 'cmb2_calculate' );

function custom_post_calculate() {
    register_post_type('calculate', array(
        'labels'             => array(
        'name'               => 'Calculate',
        'singular_name'      => 'Calculate',
        'add_new'            => 'Add New',
        'add_new_item'       => 'Add New Calculate',
        'edit_item'          => 'Edit Calculate',
        'new_item'           => 'New Calculate',
        'all_items'          => 'All Calculate',
        'view_item'          => 'View Calculate',
        'search_items'       => 'Search Calculate',
        'not_found'          => 'No Calculate found',
        'not_found_in_trash' => 'No Calculate found in the Trash',
        'menu_name'          => 'Calculate'
    ),
        'description'        => 'Calculate',
        'menu_icon'          => 'dashicons-admin-users',
        'public'             => true,
        'supports'           => array('title', 'thumbnail', 'post_tag'),
        'taxonomies'         => array('post_tag'),
        'has_archive'        => true,
        'rewrite'            => array('slug' => 'calculate')
    )); 
}

function cmb2_calculate() {
    $cmb = new_cmb2_box( array(
      'id'            => 'test_calculate',
      'title'         => __( 'Calculate', 'cmb2' ),
      'object_types'  => array( 'calculate', ),
      'context'       => 'side',
      'priority'      => 'high',
      'show_names'    => true, // Show field names on the left
    ));
    $cmb->add_field( array(
      'name'       => __( 'Field 1', 'cmb2' ),
      'id'         => 'field-1',
      'type'       => 'text',
    ));
}
?>