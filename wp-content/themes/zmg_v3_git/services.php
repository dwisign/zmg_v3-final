<?php /* Template Name: Services */ ?>

<?php get_header(); ?>

<!--BANNER INSIDE-->
<div class="banner banner-inside">
  <div class="container">
    <div class="row">
      <div class="col-lg-4">
        <div class="right-side">
          <h4 class="title"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>&nbsp <?php the_title(); ?></h4><hr>            
          <p class="desc-title-inside">
            <?php the_field('caption_top');?>
          </p>
        </div>
      </div>
      <div class="col-lg-8">
        <img src="<?php the_field('image_banner_top');?>" width="100%">   
      </div>
    </div>
  </div>
</div> 
<!--END BANNER INSIDE-->


<!--CONTENT TWO COLUMN-->
<div class="content-full-inside">
  <div class="container">
    <div class="row">
      <!-- LEFT SIDE -->
      <div class="col-sm-12">
      	<div class="content-wrap">
	        <div class="left-side">
	          <?php get_breadcrumb(); ?><hr>
	          

            <?php
              $args = array(
                'post_type' => 'service',
                'posts_per_page' => 4
              );
              $counter = 0;
              $service = new WP_Query( $args );
            ?>

            <?php while ($service->have_posts()): $service->the_post() ?>
                <?php if ($service->current_post % 2 == 0): ?>
                    <div class="item-services">
                      <div class="row">
                        <div class="col-md-5 col-md-push-7 right">
                          <div class="panel panel-default">
                            <div class="panel-heading">
                              <img src="<?php the_field('item_image');?>" alt="zmgServices" width="100%">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-7 col-md-pull-5 left desc">
                          <div class="panel-body">
                            <h5 class="title"><?php the_field('item_title');?></h5>
                            <p>
                              <?php the_field('item_desc');?>
                            </p>
                            <div class="label label-default"><?php the_field('label_grey');?></div>
                            <div class="label label-primary"><?php the_field('label_dark_blue');?></div>
                            <div class="label label-success"><?php the_field('label_green');?></div>
                            <div class="label label-info"><?php the_field('label_light_blue');?></div>
                          </div>
                        </div>   
                      </div>
                    </div>
                <?php else: ?>
                    <div class="item-services">
                      <div class="row">
                        <div class="col-md-7 left">
                          <div class="panel panel-default">
                            <div class="panel-heading">
                              <img src="<?php the_field('item_image');?>" alt="zmgServices" width="100%">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-5 right desc">
                          <div class="panel-body">
                            <h5 class="title"><?php the_field('item_title');?></h5>
                            <p>
                              <?php the_field('item_desc');?>
                            </p>
                            <div class="label label-default"><?php the_field('label_grey');?></div>
                            <div class="label label-primary"><?php the_field('label_dark_blue');?></div>
                            <div class="label label-success"><?php the_field('label_green');?></div>
                            <div class="label label-info"><?php the_field('label_light_blue');?></div>
                          </div>
                        </div>   
                      </div>
                    </div>
                <?php endif ?>
            <?php endwhile ?>
	        </div>         
        </div>
      </div>
      <!-- END LEFT SIDE -->
    </div>
  </div><!--END CONTAINER-->
</div> 
<!--END CONTENT TWO COLUMN-->


<?php get_footer(); ?>