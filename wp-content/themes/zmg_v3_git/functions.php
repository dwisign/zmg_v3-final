<?php

//Load Theme CSS
function theme_styles() {
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );
	wp_enqueue_style( 'googlefont', 'https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,400,300,700' );
	wp_enqueue_style( 'main', get_template_directory_uri() . '/style.css' );

	wp_register_style( 'owl-carousel', get_template_directory_uri() . '/css/owl-carousel/owl.carousel.css' );
	wp_register_style( 'owl-theme', get_template_directory_uri() . '/css/owl-carousel/owl.theme.css' );
	wp_register_style( 'owl-transitions', get_template_directory_uri() . '/css/owl-carousel/owl.transitions.css' );
	wp_register_style( 'prettify', get_template_directory_uri() . '/css/owl-carousel/google-code-prettify/prettify.css' );

	if( is_page('home') ){
		wp_enqueue_style( 'owl-carousel' );
		wp_enqueue_style( 'owl-theme' );
		wp_enqueue_style( 'owl-transitions' );
		wp_enqueue_style( 'prettify' );
	}
}

//Load Theme JS
function theme_js() {
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap/bootstrap.min.js', array('jquery'), 'false', true );
	
	wp_register_script( 'owl', get_template_directory_uri() . '/js/owl-carousel/owl.carousel.min.js', array('jquery'), 'false', true );
	wp_register_script( 'prettify', get_template_directory_uri() . '/js/google-code-prettify/prettify.js', array('jquery'), 'false', true );
	if( is_page( 'home' ) ){
	 	wp_enqueue_script( 'owl' );
	 	wp_enqueue_script( 'prettify' );	
	}
	wp_enqueue_script( 'zmg', get_template_directory_uri() . '/js/zmg.js', array('jquery'), 'false', true );
}
add_action( 'wp_enqueue_scripts', 'theme_js' );
add_action( 'wp_enqueue_scripts', 'theme_styles' );

//Register Custom Navigation Walker
require_once('wp_bootstrap_navwalker.php');
register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'ZMG' ),
));

//enable custom menu
add_theme_support( 'menus' );

//enable breadcrumb
function get_breadcrumb() {
    echo '<a href="'.home_url().'" rel="nofollow">Home</a>';
    if (is_category() || is_single()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
        the_category(' &bull; ');
            if (is_single()) {
                echo " &nbsp;&nbsp;&#187;&nbsp;&nbsp; ";
                the_title();
            }
    } elseif (is_page()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
        echo the_title();
    } elseif (is_search()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;Search Results for... ";
        echo '"<em>';
        echo the_search_query();
        echo '</em>"';
    }
}

//remove bug jquery dependencies
add_action( 'wp_default_scripts', function( $scripts ) {
	if ( ! empty( $scripts->registered['jquery'] ) ) {
		$jquery_dependencies = $scripts->registered['jquery']->deps;
		$scripts->registered['jquery']->deps = array_diff( $jquery_dependencies, array( 'jquery-migrate' ) );
	}
});


//add custom post services
function my_custom_post_services() {

  $labels = array(
    'name'               => _x( 'Services', 'post type general name' ),
    'singular_name'      => _x( 'Service', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New Service' ),
    'edit_item'          => __( 'Edit Service' ),
    'new_item'           => __( 'New Service' ),
    'all_items'          => __( 'All Services' ),
    'view_item'          => __( 'View Service' ),
    'search_items'       => __( 'Search Services' ),
    'not_found'          => __( 'No services found' ),
    'not_found_in_trash' => __( 'No services found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Services'
  );

  $args = array(
  	'labels'        => $labels,
    'description'   => 'Holds our services and service specific data',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
    'has_archive'   => true,
  );
  register_post_type( 'service', $args ); 
}
add_action( 'init', 'my_custom_post_services' );

//add services message
function my_updated_messages( $messages ) {
  global $post, $post_ID;
  $messages['service'] = array(
    0 => '', 
    1 => sprintf( __('Service updated. <a href="%s">View service</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Service updated.'),
    5 => isset($_GET['revision']) ? sprintf( __('Service restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Service published. <a href="%s">View service</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Service saved.'),
    8 => sprintf( __('Service submitted. <a target="_blank" href="%s">Preview service</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Service scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview service</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Service draft updated. <a target="_blank" href="%s">Preview service</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}
add_filter( 'post_updated_messages', 'my_updated_messages' );

//add custom post taxonomies for custom post type
function my_taxonomies_service() {
  $labels = array(
    'name'              => _x( 'Categories', 'taxonomy general name' ),
    'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Categories' ),
    'all_items'         => __( 'All Categories' ),
    'parent_item'       => __( 'Parent Category' ),
    'parent_item_colon' => __( 'Parent Category:' ),
    'edit_item'         => __( 'Edit Category' ), 
    'update_item'       => __( 'Update Category' ),
    'add_new_item'      => __( 'Add New Category' ),
    'new_item_name'     => __( 'New Category' ),
    'menu_name'         => __( 'Categories' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );    
  register_taxonomy( 'service_category','service', $args );
}
add_action( 'init', 'my_taxonomies_service', 0 );

//add custom post partner
function my_custom_post_partner() {

  $labels = array(
    'name'               => _x( 'Partners', 'post type general name' ),
    'singular_name'      => _x( 'partner', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New Partner' ),
    'edit_item'          => __( 'Edit Partner' ),
    'new_item'           => __( 'New Partner' ),
    'all_items'          => __( 'All Partner' ),
    'view_item'          => __( 'View Partner' ),
    'search_items'       => __( 'Search Partners' ),
    'not_found'          => __( 'No Partners found' ),
    'not_found_in_trash' => __( 'No Partners found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Partners'
  );

  $args = array(
    'labels'        => $labels,
    'description'   => 'Holds our partners and partner specific data',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
    'has_archive'   => true,
  );
  register_post_type( 'partner', $args ); 
}
add_action( 'init', 'my_custom_post_partner' );

//add partner message
function my_updated_messages_partner( $messages ) {
  global $post, $post_ID;
  $messages['partner'] = array(
    0 => '', 
    1 => sprintf( __('Partner updated. <a href="%s">View partner</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Partner updated.'),
    5 => isset($_GET['revision']) ? sprintf( __('Partner restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Partner published. <a href="%s">View partner</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Partner saved.'),
    8 => sprintf( __('Partner submitted. <a target="_blank" href="%s">Preview partner</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Partner scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview partner</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Partner draft updated. <a target="_blank" href="%s">Preview partner</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}
add_filter( 'post_updated_messages', 'my_updated_messages_partner' );

//add custom post taxonomies for custom post type
function my_taxonomies_partner() {
  $labels = array(
    'name'              => _x( 'Categories', 'taxonomy general name' ),
    'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Categories' ),
    'all_items'         => __( 'All Categories' ),
    'parent_item'       => __( 'Parent Category' ),
    'parent_item_colon' => __( 'Parent Category:' ),
    'edit_item'         => __( 'Edit Category' ), 
    'update_item'       => __( 'Update Category' ),
    'add_new_item'      => __( 'Add New Category' ),
    'new_item_name'     => __( 'New Category' ),
    'menu_name'         => __( 'Categories' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );    
  register_taxonomy( 'partner_category','partner', $args );
}
add_action( 'init', 'my_taxonomies_partner', 0 );

//add custom post thumbnails
add_theme_support( 'post-thumbnails' ); 

//add sidebar-widget
function arphabet_widgets_init() {
    register_sidebar( array(
        'name'          => 'Sidebar',
        'id'            => 'sidebar_1',
        'before_widget' => '<div id="job-list">',
        'after_widget'  => '</div><br/>',
        'before_title'  => '<h5 class="title">',
        'after_title'   => '</h5><hr>',
    ));
    register_sidebar( array(
        'name'          => 'Footer 2',
        'id'            => 'footer_2',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h5 class="title">',
        'after_title'   => '</h5>',
    ));
    register_sidebar( array(
        'name'          => 'Footer 3',
        'id'            => 'footer_3',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h5 class="title">',
        'after_title'   => '</h5>',
    ));
    register_sidebar( array(
        'name'          => 'Footer 4',
        'id'            => 'footer_4',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h5 class="title">',
        'after_title'   => '</h5>',
    ));
}
add_action( 'widgets_init', 'arphabet_widgets_init' );

//remove menu for user role dashboard
add_action( 'admin_init', 'my_remove_menu_pages' );
function my_remove_menu_pages() {
    global $user_ID;
    if ( current_user_can( 'editor' ) ) {
        remove_menu_page( 'profile.php' );
        remove_menu_page( 'tools.php' );
        remove_menu_page( 'edit-comments.php' );
        remove_menu_page( 'edit.php?post_type=service' );
        remove_menu_page( 'edit.php?post_type=partner' );
        remove_menu_page( 'edit.php?post_type=page' );
        remove_menu_page( 'wpcf7' );
        remove_menu_page( 'upload.php' );
    }
}

add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_links' );
function remove_admin_bar_links() {
    global $wp_admin_bar, $current_user;    
    if ($current_user->ID != 1) {
        $wp_admin_bar->remove_menu('wp-logo');
        $wp_admin_bar->remove_menu('comments');
        $wp_admin_bar->remove_menu('new-content');
        $wp_admin_bar->remove_menu('edit');
    }
}

//add pagination
function wp_bs_pagination($pages = '', $range = 4){  
     $showitems = ($range * 2) + 1;  
     global $paged;
     if(empty($paged)) $paged = 1;
     if($pages == ''){
         global $wp_query; 
         $pages = $wp_query->max_num_pages;
         if(!$pages){
             $pages = 1;
         }
     }   
     if(1 != $pages){
        echo '<div class="text-center">'; 
        echo '<nav><ul class="pagination pagination-sm"><li class="disabled hidden-xs"><span><span aria-hidden="true">Page '.$paged.' of '.$pages.'</span></span></li>';
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link(1)."' aria-label='First'>&laquo;<span class='hidden-xs'> First</span></a></li>";
         if($paged > 1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged - 1)."' aria-label='Previous'>&lsaquo;<span class='hidden-xs'> Previous</span></a></li>";
         for ($i=1; $i <= $pages; $i++){
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
                 echo ($paged == $i)? "<li class=\"active\"><span>".$i." <span class=\"sr-only\">(current)</span></span>
                 </li>":"<li><a href='".get_pagenum_link($i)."'>".$i."</a></li>";
             }
         }
         if ($paged < $pages && $showitems < $pages) echo "<li><a href=\"".get_pagenum_link($paged + 1)."\"  aria-label='Next'><span class='hidden-xs'>Next </span>&rsaquo;</a></li>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($pages)."' aria-label='Last'><span class='hidden-xs'>Last </span>&raquo;</a></li>";
         echo "</ul></nav>";
         echo "</div>";
     }
}
?>