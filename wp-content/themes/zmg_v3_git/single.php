<?php get_header(); ?>
	
	<!-- BANNER INSIDE -->
    <div class="banner banner-inside">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <div class="right-side">
              <h4 class="title"><span class="glyphicon glyphicon-star" aria-hidden="true"></span>&nbsp CAREER</h4><hr>            
              <p class="desc-title-inside">
                "We are a fast growing company now is seeking for best 
                candidates to join us for promotion together" 
              </p>
            </div>
          </div>
          <div class="col-lg-8">
            <img src="<?php bloginfo('template_directory'); ?>/image/banner-inside-3.jpg" width="100%">   
          </div>
        </div>
      </div>
    </div>
    <!-- END BANNER INSIDE --> 

	<!-- CONTENT TWO COLUMN -->
    <div class="content-two-column">
      <div class="container">
        <div class="row">
          <!-- LEFT SIDE -->
          <div class="col-sm-8">
            <div class="left-side">
              <!-- <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">Career</li>
              </ol> -->
              <?php get_breadcrumb(); ?><hr>
              <!-- MAIN JOB LIST -->
              <div class="panel panel-default content-page">
                <div class="panel-body">
                  <?php if ( have_posts () ) : while ( have_posts () ) : the_post ();?>
                  <h4 class="list-group-item-heading"><b><?php the_title(); ?></b></h4>
                  <p class="list-group-item-text"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> <?php the_field( 'location' );?> | <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Valid Until : <?php the_field( 'close_date' );?></p>
                  <hr>
                  <h5><b>Responsibility</b></h5>
                  <p><?php the_field( 'responsibility' );?></p>
                  </br>
                  <h5><b>Requirements</b></h5>
                  <p><?php the_field( 'requirement' );?></p>
                  </br>
                  <h5><b>Additional Information</b></h5>
                  <p><?php the_field( 'information' );?></p>
                  </br></br>

                  <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#applyJob">APPLY NOW</button><br/><hr>
                  <p class="small">Please put your photo into your CV, mention availability and your expected salary, also write down on subject the position applied and your domicile. Email to recruitment@zmg.co.id </p><br/>
                  <?php endwhile; else: ?>
                  <?php endif; ?>


                </div>
              </div>
              <!-- END JOB LIST -->

            </div>
          </div>
          <!-- END LEFT SIDE -->

          <!-- RIGHT SIDE -->
          <div class="col-sm-4">
            <div class="right-side">
              <!-- RIGHT SIDE WIDGET -->
              <?php get_sidebar(); ?>
              <!-- END RIGHT SIDE WIDGET -->
            </div>
          </div>
          <!-- END RIGHT SIDE -->
        </div>
      </div><!-- END CONTAINER -->
    </div>
    <!-- END CONTENT TWO COLUMN -->

<?php get_template_part( 'modal' ); ?>

<?php get_footer(); ?>