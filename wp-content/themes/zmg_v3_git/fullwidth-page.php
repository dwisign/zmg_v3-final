<?php /* Template Name: Full Width */ ?>

<?php get_header(); ?>

<!--BANNER INSIDE-->
<div class="banner banner-inside">
  <div class="container">
    <div class="row">
      <div class="col-lg-4">
        <div class="right-side">
          <h4 class="title"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>&nbsp <?php the_title(); ?></h4><hr>            
          <p class="desc-title-inside">
            "Leading professional firm which oriented business 
            in Engineering and Construction of a global cellular mobile 
            communication system." 
          </p>
        </div>
      </div>
      <div class="col-lg-8">
        <img src="<?php bloginfo('template_directory'); ?>/image/banner-inside-2.jpg" width="100%">   
      </div>
    </div>
  </div>
</div> 
<!--END BANNER INSIDE-->


<!--CONTENT ONE COLUMN-->
<div class="content-full-inside">
  <div class="container">
    <div class="row">
      <!-- LEFT SIDE -->
      <div class="col-sm-12">
      	<div class="content-wrap">
	        <div class="left-side">
	          <?php get_breadcrumb(); ?><hr>
	          <?php if ( have_posts () ) : while ( have_posts () ) : the_post ();?>
	          <p>
	            <?php the_content();?>
	          </p>
	          <?php endwhile; else: ?>
	          <?php endif; ?>
	        </div>
        </div>
      </div>
      <!-- END LEFT SIDE -->
    </div>
  </div><!--END CONTAINER-->
</div> 
<!--END CONTENT TWO COLUMN-->


<?php get_footer(); ?>