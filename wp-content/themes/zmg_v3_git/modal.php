<!-- Modal -->
<div class="modal fade" id="applyJob" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Apply Job</h4>
      </div>
      <div class="modal-body">
        <p>Please complete the form bellow.</p>
        <?php echo do_shortcode( '[contact-form-7 id="201" title="Apply Jobs"]' ); ?>
      </div>
    </div>
  </div>
</div>